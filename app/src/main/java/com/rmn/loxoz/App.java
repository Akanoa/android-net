package com.rmn.loxoz;


import android.util.Log;

/**
 * Created by yguern on 20/12/2017.
 */

public class App {

    private ExecHttpRequest http;
    private MainActivity activity;

    public App() {

        http = new ExecHttpRequest();
        http.setApp(this);
    }

    public void getResult(String url) {

        http.execute(url);
    }

    public void update(String body) {
        this.activity.updateElement(body);
    }

    public void setActivity(MainActivity activity) {
        this.activity = activity;
    }
}
