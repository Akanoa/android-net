package com.rmn.loxoz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private App app;
    private TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        app = new App();
        app.setActivity(this);

        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();

        text =  findViewById(R.id.text1);

        app.getResult("http://url.com");
    }

    protected void updateElement(String content){

        text.setText(content);
    }
}
