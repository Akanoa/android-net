package com.rmn.loxoz;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by yguern on 20/12/2017.
 */

public class ExecHttpRequest extends AsyncTask<String, Void, String> {

    private App app;

    @Override
    protected String doInBackground(String... url) {

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url[0])
                .build();

        try {
            Response response = client.newCall(request).execute();
            return response.body().string();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    protected void onPostExecute(String body) {
        app.update(body);
    }

    public void setApp(App app) {
        this.app = app;
    }
}
